INSERT INTO auditorium (id, name) VALUES (1, 'Auditorium 1');
INSERT INTO auditorium (id, name) VALUES (2, 'Auditorium 2');

INSERT INTO seat (id, auditoriumId, number, vip) VALUES (1, 1, 1, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (2, 1, 2, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (3, 1, 3, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (4, 1, 4, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (5, 1, 5, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (6, 1, 6, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (7, 1, 7, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (8, 1, 8, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (9, 1, 9, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (10, 1, 10, false);

INSERT INTO seat (id, auditoriumId, number, vip) VALUES (11, 2, 1, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (12, 2, 2, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (13, 2, 3, true);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (14, 2, 4, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (15, 2, 5, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (16, 2, 6, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (17, 2, 7, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (18, 2, 8, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (19, 2, 9, false);
INSERT INTO seat (id, auditoriumId, number, vip) VALUES (20, 2, 10, false);

INSERT INTO event (name, basePrice, rating, auditoriumId) VALUES ('Event 1', 299.99, 'LOW', 1);
INSERT INTO event (name, basePrice, rating, auditoriumId) VALUES ('Event 2', 999.99, 'MID', 2);

INSERT INTO seance (eventId, dateTime) VALUES (1, {ts '2016-08-01 12:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (1, {ts '2016-08-01 16:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (1, {ts '2016-08-01 15:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (1, {ts '2016-08-01 20:00:00.00'});

INSERT INTO seance (eventId, dateTime) VALUES (2, {ts '2016-08-01 13:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (2, {ts '2016-08-01 18:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (2, {ts '2016-08-01 09:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (2, {ts '2016-08-01 12:00:00.00'});
INSERT INTO seance (eventId, dateTime) VALUES (2, {ts '2016-08-01 17:00:00.00'});

INSERT INTO user (firstName, lastName, birthday, email, password) VALUES ('Ivan', 'Ivanov', '1990-01-01', 'john@srv.com', '$2a$10$6MkEteMxpGXy6WpE3Ty0q.qhwtZjdMy42lQACk1p.fXn82eM448PS');
INSERT INTO user (firstName, lastName, birthday, email, password) VALUES ('Test', 'Smith', '1989-08-01', 'b@b.com', '$2a$10$6MkEteMxpGXy6WpE3Ty0q.qhwtZjdMy42lQACk1p.fXn82eM448PS');

INSERT INTO role (id, code, label) VALUES (1, 'ROLE_REGISTERED_USER', 'User');
INSERT INTO role (id, code, label) VALUES (2, 'ROLE_BOOKING_MANAGER', 'Booking manager');

INSERT INTO userRole (userId, roleId) SELECT u.id, r.id FROM user u, role r WHERE u.email = 'john@srv.com' AND r.id = 2;
INSERT INTO userRole (userId, roleId) SELECT u.id, r.id FROM user u, role r WHERE u.email = 'b@b.com' AND r.id = 1;
INSERT INTO userRole (userId, roleId) SELECT u.id, r.id FROM user u, role r WHERE u.email = 'b@b.com' AND r.id = 2;