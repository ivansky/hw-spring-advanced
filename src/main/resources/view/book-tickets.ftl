<#include "layout/main.ftl">

<@main title="Book tickets">

<div>
    <form action="/booking/book-tickets/book">
        <div>
            <select name="userId" onchange="this.form.submit()" class="selectpicker" title="Choose user..">
                <#list users as user>
                    <option value="${user.id}" <#if selectedUser?? && user.id == selectedUser.id> selected </#if>>
                        ${user.name}
                    </option>
                </#list>
            </select>
        </div>

        <#if selectedUser??>
            <div>
                <select name="eventId" onchange="this.form.submit()" class="selectpicker" title="Choose event..">
                    <#list events as event>
                        <option value="${event.id}" <#if selectedEvent?? && event.id == selectedEvent.id> selected </#if>>${event.name}</option>
                    </#list>
                </select>
            </div>
        </#if>

        <#if selectedEvent??>
            <div class="input-group">
                <span class="input-group-addon">Seats</span>
                <input type="text" class="form-control" placeholder="Seats" name="seats"/>
            </div>
            <button class="btn btn-default" type="submit">Book</button>
        </#if>
    </form>
</div>

</@main>