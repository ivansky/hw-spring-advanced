<#macro main title>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${title}</title>

    <#include "../include/scripts.ftl">
    <#include "../include/styles.ftl">
</head>
<body>

    <#include "../include/navbar.ftl">

    <div class="content">
        <#nested>
    </div>

</body>
</html>
</#macro>