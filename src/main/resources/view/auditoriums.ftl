<#include "layout/main.ftl">

<@main title="Auditoriums">
<ul class="list-group">
    <#list auditoriums as auditorium>
        <li class="list-group-item">
            <h4>${auditorium.name}</h4>
            <p class="list-group-item-text">

            </p>
            <div class="list-group-item-text">
                <#list auditorium.seats as seat>
                    <#if seat.isVip()>
                        <button class="btn btn-warning">${seat.getNumber()}</button>
                    <#else>
                        <button class="btn btn-default">${seat.getNumber()}</button>
                    </#if>
                </#list>
            </div>
        </li>
    <#else>
        <li>
            Haven't auditoriums
        </li>
    </#list>
</ul>

</@main>
