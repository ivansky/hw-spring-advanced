<#include "layout/main.ftl">

<@main title="Login">

<#if Session.SPRING_SECURITY_LAST_EXCEPTION?? && Session.SPRING_SECURITY_LAST_EXCEPTION.message?has_content>
    <div class="alert alert-danger" role="alert">${Session.SPRING_SECURITY_LAST_EXCEPTION.message}</div>
</#if>

<form method="post">

    <#if _csrf??>
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
    </#if>

    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input name="username" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="checkbox">
        <label>
            <input name="remember-me" type="checkbox"> Remember me
        </label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>

</@main>