<#include "layout/main.ftl">

<@main title="Users">

<form action="/users" method="post" enctype="multipart/form-data">
    <label class="control-label">Select File</label>
    <input id="input-1a" type="file" class="file" name="file" data-show-preview="false">
</form>

<form>
    <div class="input-group">
        <span class="input-group-addon" id="email">@</span>
        <input name="email" type="text" class="form-control" placeholder="Email" aria-describedby="email" <#if email??>value="${email}"</#if>/>
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Search</button>
        </span>
    </div>
</form>

<div class="panel panel-default">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>first name</th>
            <th>last name</th>
            <th>email</th>
            <th>birthday</th>
        </tr>
        </thead>
        <tbody>
        <#if users??>
            <#list users as user>
                <tr>
                    <td scope="row">${user.id}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.email}</td>
                    <td>${user.birthday}</td>
                </tr>
            <#else>
                <tr>
                    <td colspan="4">Haven't users</td>
                </tr>
            </#list>
        <#else>
            <tr><td colspan="4">Can not found users</td></tr>
        </#if>
        </tbody>
    </table>
</div>

</@main>
