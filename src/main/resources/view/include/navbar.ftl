<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Home</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/auditoriums">Auditoriums</a></li>
                <li><a href="/events">Events</a></li>
                <li><a href="/users">Users</a></li>
                <li><a href="/booking">Booking</a></li>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <#if userName??>
                    <li><a href="#">${userName}</a></li>
                    <li><a href="/logout" onclick="document.getElementById('logout').submit();return false;">Logout</a></li>
                <#else>
                    <li><a href="/login">Login</a></li>
                </#if>
            </ul>
            <form id="logout" action="/logout" method="post" >
                <#if _csrf??>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </#if>
            </form>
        </div>
    </div>
</nav>
