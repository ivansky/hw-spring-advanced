<#include "layout/main.ftl">

<@main title="Events">

    <form action="/events" method="post" enctype="multipart/form-data">
        <label class="control-label">Select File</label>
        <input id="input-1a" type="file" class="file" name="file" data-show-preview="false">
    </form>

    <form>
        <div class="input-group">
            <span class="input-group-addon" id="name">Name</span>
            <input name="name" type="text" class="form-control" placeholder="Event name" <#if name??> value="${name}" </#if>/>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Search</button>
            </span>
        </div>
    </form>

    <div class="panel panel-default">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>name</th>
                <th>rating</th>
                <th>price</th>
            </tr>
            </thead>
            <tbody>

            <#list events as event>
            <tr>
                <td scope="row">${event.id}</td>
                <td>${event.name}</td>
                <td>${event.rating}</td>
                <td>${event.basePrice}</td>
            </tr>
            <#else>
            <tr>
                <td colspan="4">Haven't events</td>
            </tr>
            </#list>

            </tbody>
        </table>
    </div>

</@main>
