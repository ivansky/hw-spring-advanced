<#include "layout/main.ftl">

<@main title="Error">
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <#if errorMessage??>
            ${errorMessage}
        <#else>
            Undefined error
        </#if>
    </div>
</@main>
