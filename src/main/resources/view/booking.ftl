<#include "layout/main.ftl">

<@main title="Booking">

<ul class="list-group">
    <li class="list-group-item"><a href="/booking/book-tickets">Book tickets</a></li>
</ul>

<h2>Get Purchased tickets:</h2>
<form id="EventAndDatetimeForm" action="/booking/tickets" method="post">
    <div>
        <select id="selectEvent" name="eventId" class="selectpicker" title="Choose event and date..">
            <#list events as event>
                <option value="${event.id}">${event.name} - ${event.dateTime}</option>
            </#list>
        </select>
    </div>
</form>

<script type="text/javascript">
$("#selectEvent").on('change', function() {
  //var eventId = $(this.options[this.selectedIndex]).closest('optgroup').attr('value');
  //$('#eventId').val(eventId);
  $('#EventAndDatetimeForm').submit();
});
</script>

</@main>