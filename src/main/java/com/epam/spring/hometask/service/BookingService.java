package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

public interface BookingService {

  double getTicketsPrice(@Nonnull Seance seance, @Nullable User user, @Nonnull Set<Long> seats);

  void bookTickets(@Nonnull Set<Ticket> tickets, @Nonnull User user);

  Collection<Ticket> getTicketsForSeance(Seance seance, LocalDateTime dateTime);

  Collection<Ticket> getPurchasedTickets();

}
