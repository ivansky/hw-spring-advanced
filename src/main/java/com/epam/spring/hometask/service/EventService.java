package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.EventRating;
import com.epam.spring.hometask.service.exception.AuditoriumAlreadyAssignedException;
import com.epam.spring.hometask.service.exception.EntityNotFoundException;
import com.epam.spring.hometask.service.exception.InternalServerException;

import java.util.Collection;

public interface EventService {

  Event create(String name, float price, EventRating rating);

  Event save(Event event);

  Collection<Event> save(Collection<Event> event);

  void remove(Event event);

  Event getByName(String name);

  Event getById(long id) throws EntityNotFoundException;

  Collection<Event> getAll() throws InternalServerException;

  Event assignAuditorium(Event event, Auditorium auditorium) throws AuditoriumAlreadyAssignedException;

}
