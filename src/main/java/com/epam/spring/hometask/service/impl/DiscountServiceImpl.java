package com.epam.spring.hometask.service.impl;

import java.util.List;

import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.strategy.DiscountStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.DiscountService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Service
public class DiscountServiceImpl implements DiscountService {

  @Autowired
  private List<DiscountStrategy> discountStrategies;

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Seance seance, Long numberOfTickets) {
    return discountStrategies
        .stream()
        .map(discountStrategy -> discountStrategy.getDiscount(user, seance, numberOfTickets))
        .max(Byte::compare)
        .get();
  }

}
