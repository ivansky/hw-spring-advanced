package com.epam.spring.hometask.service.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.epam.spring.hometask.domain.EventRating;
import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.service.exception.EntityNotFoundException;
import com.epam.spring.hometask.service.exception.InternalServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.repository.EventRepository;
import com.epam.spring.hometask.service.EventService;
import com.epam.spring.hometask.service.exception.AuditoriumAlreadyAssignedException;

import javax.annotation.Nonnull;

@Service
public class EventServiceImpl implements EventService {

  private static final Logger log = LoggerFactory.getLogger(EventService.class);

  @Autowired
  private EventRepository eventRepository;

  @Override
  public Event create(String name, float price, EventRating rating) throws InternalServerException {
    Event event = new Event(name, price, rating);
    log.info("Event <" + event.getName() + "> created");
    try {
      return eventRepository.save(event);
    } catch (Exception e) {
      throw new InternalServerException("Can not save event", e);
    }
  }

  @Override
  public Event save(Event user) throws InternalServerException {
    log.info("User <" + user.getName() + "> is registered");
    try {
      user = eventRepository.save(user);
    } catch (Exception e) {
      throw new InternalServerException("Can not save user", e);
    }
    return user;
  }

  @Override
  public Collection<Event> save(Collection<Event> events) {
    try {
      return eventRepository.save(events);
    } catch (Exception e) {
      throw new InternalServerException();
    }
  }

  @Override
  public void remove(@Nonnull Event event) {
    if (event.getId() != null) {
      try {
        eventRepository.delete(event.getId());
      } catch (Exception e) {
        throw new InternalServerException("Can not delete event", e);
      }
    }
  }

  @Override
  public Event getByName(String name) {
    return eventRepository.findByName(name);
  }

  @Override
  public Event getById(long id) throws EntityNotFoundException {
    try {
      return eventRepository.findOne(id);
    } catch (Exception e) {
      throw new EntityNotFoundException("Event not found", e);
    }
  }

  @Override
  public Collection<Event> getAll() throws InternalServerException {
    try {
      return eventRepository.findAll();
    } catch (Exception e) {
      throw new InternalServerException("Can not find events", e);
    }
  }

  @Override
  public Event assignAuditorium(Event event, Auditorium auditorium) throws AuditoriumAlreadyAssignedException {

    if(event == null) {
      throw new EntityNotFoundException("Event not found");
    }

    Set<LocalDateTime> alreadyAssignedDateTimes = new HashSet<>();

    eventRepository
        .findAllByAuditoriumId(auditorium.getId())
        .forEach(e -> e.getSeances().stream().forEach(s -> alreadyAssignedDateTimes.add(s.getDateTime())));

    List<Seance> conflictSeances = event.getSeances().stream()
        .filter(s -> alreadyAssignedDateTimes.stream()
            .filter(dateTime -> s.getDateTime().equals(dateTime)).count() > 0)
        .collect(Collectors.toList());

    if(conflictSeances.size() > 0) {
      throw new AuditoriumAlreadyAssignedException(event, auditorium);
    }

    event.setAuditorium(auditorium);

    try {
      eventRepository.save(event);
    } catch (Exception e) {
      throw new InternalServerException("Can not save event", e);
    }

    log.info("Event {} assigned to auditorium {}", event.getName(), auditorium.getName());

    return event;
  }

}
