package com.epam.spring.hometask.service.exception;


public class InsufficientFundsException extends RuntimeException {

  public InsufficientFundsException() {
    super("User account haven't enough money");
  }

}
