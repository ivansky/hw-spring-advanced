package com.epam.spring.hometask.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Something was broken")
public class InternalServerException extends ServiceException {

  public InternalServerException() {
    super();
  }

  public InternalServerException(String message, Throwable cause) {
    super(message, cause);
  }

}
