package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Seance;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface SeanceService {

  Seance getSeanceByEventIdAndDateTime(Long eventId, LocalDateTime dateTime);

  Seance getById(Long seanceId);

  Map<Event, List<Seance>> getForDateRange(LocalDateTime from, LocalDateTime to);
}
