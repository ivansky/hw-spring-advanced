package com.epam.spring.hometask.service.exception;

import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;

public class AuditoriumAlreadyAssignedException extends Exception {
    private String message;

    public AuditoriumAlreadyAssignedException(Event event, Auditorium auditorium) {
        message = String.format("Event <%s> is not assigned to auditorium <%s> because it's already assigned to another event",
                event.getName(),
                auditorium.getName());
    }

    @Override
    public String getMessage() {
        return message;
    }
}
