package com.epam.spring.hometask.service.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.epam.spring.hometask.service.exception.EntityNotFoundException;
import com.epam.spring.hometask.service.exception.InternalServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.repository.TicketRepository;
import com.epam.spring.hometask.repository.UserRepository;
import com.epam.spring.hometask.service.UserService;

@Service
public class UserServiceImpl implements UserService {

  private static final Logger log = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private TicketRepository ticketRepository;

  @Autowired
  private UserRepository userRepository;

  @Override
  public User save(User user) throws InternalServerException {
    try {
      user = userRepository.save(user);
      log.info("User {} {} is registered", user.getFirstName(), user.getLastName());
    } catch (Exception e) {
      throw new InternalServerException("Can not save user", e);
    }
    return user;
  }

  @Override
  public Collection<User> save(Collection<User> users) {
    try {
      return userRepository.save(users);
    } catch (Exception e) {
      throw new InternalServerException();
    }
  }

  @Override
  public void remove(User user) {
    Optional.ofNullable(user).ifPresent(u -> {
      try {
        userRepository.delete(u.getId());
      } catch (Exception e) {
        throw new InternalServerException("Can not remove user", e);
      }
    });
  }

  @Override
  public User getById(Long id) {
    try {
      return userRepository.findOne(id);
    } catch (Exception e) {
      throw new EntityNotFoundException("User not found", e);
    }
  }

  @Override
  public User getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public User getUserByName(String name) {
    User user = userRepository.findByFirstName(name);

    if(user == null) {
      user = userRepository.findByLastName(name);
    }

    return user;
  }

  @Override
  public Collection<User> getUsers() {
    try {
      return userRepository.findAll();
    } catch (Exception e) {
      throw new InternalServerException("Can not get list of users", e);
    }
  }

  @Override
  public Collection<Ticket> getTicketsByUserId(Long userId) {
    try {
      return ticketRepository.findAllByUserId(userId).collect(Collectors.toList());
    } catch (Exception e) {
      throw new EntityNotFoundException("Can not find tickets by user id", e);
    }
  }
}
