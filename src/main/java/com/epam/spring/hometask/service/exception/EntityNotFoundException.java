package com.epam.spring.hometask.service.exception;


public class EntityNotFoundException extends ServiceException {

  public EntityNotFoundException() {
    super();
  }

  public EntityNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public EntityNotFoundException(String message) {
    super(message);
  }
}
