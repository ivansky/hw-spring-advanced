package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.UserAccount;

import java.math.BigDecimal;

public interface AccountService {

  boolean checkBalanceIsGreaterThan(UserAccount account, BigDecimal amount);

  void transfer(BigDecimal amount, UserAccount fromAccount, UserAccount toAccount);

  void withdraw(UserAccount fromAccount, BigDecimal amount);

  void deposit(UserAccount toAccount, BigDecimal amount);

}
