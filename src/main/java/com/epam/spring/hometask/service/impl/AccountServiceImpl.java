package com.epam.spring.hometask.service.impl;

import com.epam.spring.hometask.domain.UserAccount;
import com.epam.spring.hometask.repository.UserAccountRepository;
import com.epam.spring.hometask.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * todo check transaction sql logs
 * https://gerrydevstory.com/2014/06/28/spring-declarative-transaction-management-with-jdk-proxy-vs-aspectj/
 */
@Service
public class AccountServiceImpl implements AccountService {

  @Autowired
  private UserAccountRepository accountRepository;

  @Transactional
  @Override
  public boolean checkBalanceIsGreaterThan(UserAccount account, BigDecimal amount) {
    return account.getAmount().compareTo(amount) >= 0;
  }

  @Transactional
  @Override
  public void transfer(BigDecimal amount, UserAccount fromAccount, UserAccount toAccount) {
    if(checkBalanceIsGreaterThan(fromAccount, amount)) {
      withdraw(fromAccount, amount);
      deposit(toAccount, amount);
    }
  }

  @Transactional
  @Override
  public void withdraw(UserAccount fromAccount, BigDecimal amount) {
    fromAccount.setAmount(fromAccount.getAmount().subtract(amount));
    accountRepository.save(fromAccount);
  }

  @Transactional
  @Override
  public void deposit(UserAccount toAccount, BigDecimal amount) {
    toAccount.setAmount(toAccount.getAmount().add(amount));
    accountRepository.save(toAccount);
  }
}
