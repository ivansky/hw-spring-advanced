package com.epam.spring.hometask.service.impl;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.repository.SeanceRepository;
import com.epam.spring.hometask.service.SeanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SeanceServiceImpl implements SeanceService {

  @Autowired
  private SeanceRepository seanceRepository;

  @Override
  public Seance getSeanceByEventIdAndDateTime(Long eventId, LocalDateTime dateTime) {
    return seanceRepository.findByEventIdAndDateTime(eventId, dateTime);
  }

  @Override
  public Seance getById(Long seanceId) {
    return seanceRepository.findOne(seanceId);
  }

  @Override
  public Map<Event, List<Seance>> getForDateRange(LocalDateTime from, LocalDateTime to) {
    return seanceRepository
        .findByDateTimeBetween(from, to)
        .collect(Collectors.groupingBy(Seance::getEvent));
  }

}
