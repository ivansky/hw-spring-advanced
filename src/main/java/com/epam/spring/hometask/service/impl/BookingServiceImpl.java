package com.epam.spring.hometask.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.epam.spring.hometask.domain.EventRating;
import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.service.AccountService;
import com.epam.spring.hometask.service.exception.InsufficientFundsException;
import com.epam.spring.hometask.service.exception.InternalServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Seat;
import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.repository.TicketRepository;
import com.epam.spring.hometask.service.BookingService;
import com.epam.spring.hometask.service.DiscountService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Service
public class BookingServiceImpl implements BookingService {

  private static final Logger log = LoggerFactory.getLogger(BookingService.class);

  @Value("${bookingService.markupForHighRatingEvent}")
  private double markupForHighRatingEvent;

  @Value("${bookingService.markupForVipSeat}")
  private double markupForVipSeat;

  @Autowired
  private TicketRepository ticketRepository;

  @Autowired
  private DiscountService discountService;

  @Autowired
  private AccountService accountService;

  @Override
  public double getTicketsPrice(@Nonnull Seance seance, @Nullable User user, @Nonnull Set<Long> seats) {
    double totalPrice = 0;

    long numberOfTickets = seats.size();

    byte discount = discountService.getDiscount(user, seance, numberOfTickets);

    Event event = seance.getEvent();

    double basePrice = event.getBasePrice();

    EventRating rating = event.getRating();

    double currentPrice = (rating.equals(EventRating.HIGH)) ? basePrice * markupForHighRatingEvent : basePrice;

    Auditorium auditorium = event.getAuditorium();

    Set<Long> vipSeats = auditorium.getSeats().stream()
        .filter(Seat::isVip)
        .map(Seat::getNumber).collect(Collectors.toSet());

    totalPrice = seats
        .stream()
        .mapToDouble(seat -> (vipSeats.contains(seat) ? basePrice * markupForVipSeat : currentPrice))
        .sum();

    totalPrice /= (discount > 0) ? ((double) discount / 100.0) : 1.0;

    return totalPrice;
  }

  @Transactional
  @Override
  public void bookTickets(@Nonnull Set<Ticket> tickets, @Nonnull User user) {
    Map<Seance, List<Ticket>> ticketsBySeances = tickets.stream()
        .collect(Collectors.groupingBy(Ticket::getSeance));

    double totalPrice = ticketsBySeances.entrySet().stream().map(e -> {
      Seance seance = e.getKey();
      List<Ticket> seanceTickets = e.getValue();

      Set<Long> seats = seanceTickets.stream()
          .map(t -> t.getSeat().getNumber())
          .collect(Collectors.toSet());

      return getTicketsPrice(seance, user, seats);
    }).mapToDouble(Double::doubleValue).sum();

    boolean bookingIsAvailable = accountService.checkBalanceIsGreaterThan(user.getUserAccount(), BigDecimal.valueOf(totalPrice));

    if (!bookingIsAvailable) {
      throw new InsufficientFundsException();
    }

    accountService.withdraw(user.getUserAccount(), BigDecimal.valueOf(totalPrice));

    ticketRepository.save(tickets);

    tickets.stream().forEach(ticket -> {
      ticket.setUser(user);
      user.getTickets().add(ticket);
    });
  }

  @Override
  public Collection<Ticket> getTicketsForSeance(Seance seance, LocalDateTime dateTime) {
    try {
      return ticketRepository.findAllBySeanceId(seance.getId()).collect(Collectors.toList());
    } catch (Exception e) {
      throw new InternalServerException("Can not find ticket for event", e);
    }
  }

  @Override
  public Collection<Ticket> getPurchasedTickets() {
    return ticketRepository.findAllByUserIdNotNull().collect(Collectors.toList());
  }

}
