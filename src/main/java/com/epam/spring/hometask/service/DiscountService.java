package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface DiscountService {

  byte getDiscount(@Nullable User user, @Nonnull Seance seance, Long numberOfTickets);

}
