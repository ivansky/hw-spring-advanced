package com.epam.spring.hometask.service.impl;

import com.epam.spring.hometask.service.exception.EntityNotFoundException;
import com.epam.spring.hometask.service.exception.InternalServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.spring.hometask.repository.AuditoriumRepository;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.service.AuditoriumService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

  @Autowired
  private AuditoriumRepository auditoriumRepository;

  @Nonnull
  @Override
  public Collection<Auditorium> getAll() {
    try {
      return StreamSupport.stream(auditoriumRepository.findAll().spliterator(), false)
          .collect(Collectors.toList());

    } catch (Exception e) {
      throw new InternalServerException("Can not find auditoriums", e);
    }
  }

  @Nullable
  @Override
  public Auditorium getByName(@Nonnull String name) {
    try {
      return auditoriumRepository.findByName(name);
    } catch (Exception e) {
      throw new EntityNotFoundException("Not found auditorium by name", e);
    }
  }
}
