package com.epam.spring.hometask.service;

import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.exception.InternalServerException;

import java.util.Collection;

public interface UserService {
  User save(User user) throws InternalServerException;

  Collection<User> save(Collection<User> user);

  void remove(User user);

  User getById(Long id);

  User getUserByEmail(String email);

  User getUserByName(String name);

  Collection<User> getUsers();

  Collection<Ticket> getTicketsByUserId(Long userId);
}
