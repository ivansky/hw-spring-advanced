package com.epam.spring.hometask.strategy;

import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface DiscountStrategy {

  String getStrategyName();

  byte getDiscount(@Nullable User user, @Nonnull Seance seance, long numberOfTickets);

}
