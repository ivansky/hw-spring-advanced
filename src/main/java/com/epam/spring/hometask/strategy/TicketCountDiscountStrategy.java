package com.epam.spring.hometask.strategy;

import com.epam.spring.hometask.domain.Seance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

@Component
public class TicketCountDiscountStrategy implements DiscountStrategy {

  private static final String NAME = "TICKET_COUNT_DISCOUNT";

  @Value("${ticketCountDiscount.discount}")
  private byte discount;

  @Value("${ticketCountDiscount.ticketCount}")
  private long ticketCount;

  public byte getDiscount() {
    return discount;
  }

  public void setDiscount(byte discount) {
    this.discount = discount;
  }

  public long getTicketCount() {
    return ticketCount;
  }

  public void setTicketCount(long ticketCount) {
    this.ticketCount = ticketCount;
  }

  @Override
  public String getStrategyName() {
    return NAME;
  }

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Seance seance, long numberOfTickets) {
    byte currentDiscount = 0;
    long ticketsWithDiscount = 0;

    if (user != null) {
      long count = numberOfTickets + (user.getTickets().size() % ticketCount);
      ticketsWithDiscount = count / ticketCount;
    } else {
      ticketsWithDiscount = numberOfTickets / ticketCount;
    }

    currentDiscount = (byte) ((ticketsWithDiscount * discount) / numberOfTickets);

    return currentDiscount;
  }

}

