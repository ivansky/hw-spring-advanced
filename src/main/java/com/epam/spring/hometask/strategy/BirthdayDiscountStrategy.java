package com.epam.spring.hometask.strategy;

import com.epam.spring.hometask.domain.Seance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Component
public class BirthdayDiscountStrategy implements DiscountStrategy {

  private static final String NAME = "BIRTHDAY_DISCOUNT";

  @Value("${birthdayDiscount.discount}")
  private byte discount;

  @Value("${birthdayDiscount.interval}")
  private byte interval;

  @Override
  public String getStrategyName() {
    return NAME;
  }

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Seance seance, long numberOfTickets) {

    byte currentDiscount = 0;

    if (user != null && user.getBirthday() != null) {
      LocalDate birthday = user.getBirthday();
      long daysLeft = ChronoUnit.DAYS.between(birthday, seance.getDateTime());
      currentDiscount = (daysLeft <= interval) ? discount : 0;
    }

    return currentDiscount;
  }
}
