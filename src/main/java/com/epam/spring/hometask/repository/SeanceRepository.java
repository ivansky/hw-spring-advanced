package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Seance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.stream.Stream;

@Repository
public interface SeanceRepository extends JpaRepository<Seance, Long> {

  Seance findByEventIdAndDateTime(Long eventId, LocalDateTime dateTime);

  @Query("SELECT s FROM Seance s WHERE s.dateTime BETWEEN ?1 AND ?2")
  Stream<Seance> findByDateTimeBetween(LocalDateTime from, LocalDateTime to);

}
