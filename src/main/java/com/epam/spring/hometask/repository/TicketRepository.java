package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

  Stream<Ticket> findAllBySeanceId(Long seanceId);

  Stream<Ticket> findAllByUserId(Long userId);

  Stream<Ticket> findAllByUserIdNotNull();

}
