package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterRepository extends JpaRepository<Counter, String> {

  @Modifying
  @Query("UPDATE Counter c SET c.count = c.count + 1 WHERE c.name = :name")
  int increment(@Param("name") String name);

  Counter findByName(String name);

}
