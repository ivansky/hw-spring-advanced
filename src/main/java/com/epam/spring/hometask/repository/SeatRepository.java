package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {

  Seat findByAuditoriumIdAndNumber(Long auditoriumId, Long number);

  Stream<Seat> findAllByAuditoriumId(Long auditoriumId);

}
