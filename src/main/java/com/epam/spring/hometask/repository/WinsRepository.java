package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Win;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface WinsRepository extends JpaRepository<Win, Long> {

  Win findByUserId(Long userId);

  Stream<Win> findAllByUserId(Long userId);

  Long deleteByUserId(Long userId);

}
