package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Auditorium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditoriumRepository extends JpaRepository<Auditorium, Long> {

  Auditorium findByName(String name);

}
