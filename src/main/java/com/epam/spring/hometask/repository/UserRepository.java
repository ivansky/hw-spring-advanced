package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends JpaRepository<User, Long> {

  User findByEmail(@Param("email") String email);

  User findByFirstName(@Param("firstName") String firstName);

  User findByLastName(@Param("lastName") String lastName);

}
