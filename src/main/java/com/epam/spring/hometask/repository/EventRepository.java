package com.epam.spring.hometask.repository;

import com.epam.spring.hometask.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

  Event findByName(String name);

  Stream<Event> findAllByAuditoriumId(Long auditoriumId);

}
