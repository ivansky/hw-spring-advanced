package com.epam.spring.hometask.config.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = {"classpath:hibernate.properties"})
public class HibernateConfiguration {

  @Value("${hibernate.dialect}")
  private String dialect;

  @Value("${hibernate.show_sql}")
  private boolean showSQL;

  public String getDialect() {
    return dialect;
  }

  public void setDialect(String dialect) {
    this.dialect = dialect;
  }

  public boolean isShowSQL() {
    return showSQL;
  }

  public void setShowSQL(boolean showSQL) {
    this.showSQL = showSQL;
  }
}
