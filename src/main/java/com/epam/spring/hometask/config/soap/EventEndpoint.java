package com.epam.spring.hometask.config.soap;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.dto.*;
import com.epam.spring.hometask.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.Collection;

@Endpoint
public class EventEndpoint {

  private static final String NAMESPACE_URI = "http://localhost:8080/ws";

  private EventService eventService;

  @Autowired
  public EventEndpoint(EventService eventService) {
    this.eventService = eventService;
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEventsRequest")
  @ResponsePayload
  public GetEventsResponse getEvents(@RequestPayload GetEventsRequest eventsRequest) {
    Collection<Event> events = eventService.getAll();
    return new GetEventsResponse(events);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEventByNameRequest")
  @ResponsePayload
  public GetEventByNameResponse getEventsByName(@RequestPayload GetEventByNameRequest eventByNameRequest) {
    Event event = eventService.getByName(eventByNameRequest.getName());
    return new GetEventByNameResponse(event);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEventByIdRequest")
  @ResponsePayload
  public GetEventByIdResponse getById(@RequestPayload GetEventByIdRequest eventByIdRequest) {
    Event event = eventService.getById(eventByIdRequest.getEventId());
    return new GetEventByIdResponse(event);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveEventRequest")
  @ResponsePayload
  public SaveEventResponse save(@RequestPayload SaveEventRequest saveEvnetRequest) {
    Event event = eventService.save(saveEvnetRequest.getEvent());
    return new SaveEventResponse(event);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeEventRequest")
  @ResponsePayload
  public RemoveEventResponse remove(@RequestPayload RemoveEventRequest removeEventRequest) {
    Event eventForRemove = new Event();
    eventForRemove.setId(removeEventRequest.getEventId());
    eventService.remove(eventForRemove);
    return new RemoveEventResponse("Event removed.");
  }

}
