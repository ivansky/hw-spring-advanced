package com.epam.spring.hometask.config;

import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.servlet.MultipartConfigElement;

@Configuration
public class ViewResolverConfig {

  @Bean
  public FreeMarkerConfigurer getFreeMarkerConfigurer() {
    FreeMarkerConfigurer freeMakerConfigurer = new FreeMarkerConfigurer();
    freeMakerConfigurer.setTemplateLoaderPath("classpath:/view/");
    return freeMakerConfigurer;
  }

  @Bean
  @Order(0)
  public ViewResolver getFreeMarkerViewResolver() {
    FreeMarkerViewResolver freeMakerViewResolver = new FreeMarkerViewResolver();
    freeMakerViewResolver.setSuffix(".ftl");
    return freeMakerViewResolver;
  }

  @Bean
  MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    factory.setMaxFileSize("5120MB");
    factory.setMaxRequestSize("5120MB");
    return factory.createMultipartConfig();
  }

  @Bean
  @Order(1)
  public ViewResolver getResourceBundleViewResolver() {
    ResourceBundleViewResolver resourceBundleViewResolver = new ResourceBundleViewResolver();
    resourceBundleViewResolver.setBasename("view");
    return resourceBundleViewResolver;
  }

}