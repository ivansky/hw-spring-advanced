package com.epam.spring.hometask.config;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class JacksonConfig {

  @Bean(name = "objectMapper")
  public ObjectMapper objectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.findAndRegisterModules();
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    return mapper;
  }

  @Bean(name = "userJsonReader")
  public ObjectReader userJsonReader(ObjectMapper objectMapper) {
    return objectMapper.readerFor(new TypeReference<List<User>>() {
    });
  }

  @Bean(name = "eventJsonReader")
  public ObjectReader eventJsonReader(ObjectMapper objectMapper) {
    return objectMapper.readerFor(new TypeReference<List<Event>>() {
    });
  }

}