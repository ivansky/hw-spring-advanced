package com.epam.spring.hometask.config;

import com.epam.spring.hometask.security.AccountAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String SECRET_KEY = "kjhIDIUhw23";

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private AccountAuthenticationProvider accountAuthenticationProvider;

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    accountAuthenticationProvider.setHideUserNotFoundExceptions(false); // show real message
    auth.authenticationProvider(accountAuthenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers("/").permitAll()
        .antMatchers("/h2/**", "/h2**", "/api/**", "/ws/**").permitAll()
        .antMatchers("/booking/**").hasRole("BOOKING_MANAGER")
        .anyRequest().hasRole("REGISTERED_USER")
        .and().formLogin().loginPage("/login").defaultSuccessUrl("/").permitAll()
        .and()
          .logout()
          .logoutSuccessUrl("/").permitAll()
        .and()
          .rememberMe()
          .tokenRepository(tokenRepository())
          .rememberMeServices(rememberMeAuthenticationProvider())
          .tokenValiditySeconds(86400)
        .and().csrf().ignoringAntMatchers("/booking/tickets");

    // h2 console
    http.csrf().disable();
    http.headers().frameOptions().disable();
  }

  @Bean
  public PersistentTokenBasedRememberMeServices rememberMeAuthenticationProvider() {
    return new PersistentTokenBasedRememberMeServices(SECRET_KEY, userDetailsService, tokenRepository());
  }

  @Bean
  public PersistentTokenRepository tokenRepository() {
    // todo Jpa Token Repository
    return new InMemoryTokenRepositoryImpl();
  }

}