package com.epam.spring.hometask.config;

import com.epam.spring.hometask.interceptor.UserProfileInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan("com.epam.spring.hometask")
@EnableAspectJAutoProxy
public class ApplicationConfig extends WebMvcConfigurationSupport {

  @Autowired
  UserProfileInterceptor userProfileInterceptor;

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Override
  protected void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(userProfileInterceptor);
  }

}
