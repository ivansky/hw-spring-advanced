package com.epam.spring.hometask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.service.AuditoriumService;

import java.util.Collection;

@Controller
public class AuditoriumController {

  @Autowired
  private AuditoriumService auditoriumService;

  @RequestMapping("/auditoriums")
  public String showAuditoriums(Model model) {
    Collection<Auditorium> auditoriums = auditoriumService.getAll();
    model.addAttribute("auditoriums", auditoriums);
    return "auditoriums";
  }
}
