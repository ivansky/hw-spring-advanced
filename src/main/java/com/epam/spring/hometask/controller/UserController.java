package com.epam.spring.hometask.controller;

import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.UserService;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

  private static final String USER_TEMPLATE = "users";

  @Autowired
  private UserService userService;

  @Autowired
  @Qualifier("userJsonReader")
  private ObjectReader jsonReader;

  @RequestMapping(method = RequestMethod.GET)
  public String showUsers(Model model) {
    Collection<User> users;
    users = userService.getUsers();
    model.addAttribute("users", users);
    return USER_TEMPLATE;
  }

  @RequestMapping(params = "email", method = RequestMethod.GET)
  public String getUserByEmail(Model model, @RequestParam("email") String email) {
    User user = userService.getUserByEmail(email);
    model.addAttribute("user", user);
    model.addAttribute("email", email);
    return USER_TEMPLATE;
  }

  @RequestMapping(method = RequestMethod.POST)
  public String loadUsersFromFile(Model model, @RequestParam("file") MultipartFile file) throws IOException {
    List<User> users = jsonReader.readValue(file.getInputStream());
    userService.save(users);

    return showUsers(model);
  }

}
