package com.epam.spring.hometask.controller;

import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Seance;
import com.epam.spring.hometask.domain.Seat;
import com.epam.spring.hometask.service.SeanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.BookingService;
import com.epam.spring.hometask.service.EventService;
import com.epam.spring.hometask.service.UserService;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/booking")
public class BookingController {

  private static final Logger log = LoggerFactory.getLogger(BookingController.class);

  private final BookingService bookingService;
  private final EventService eventService;
  private final UserService userService;
  private final SeanceService seanceService;

  @Autowired
  public BookingController(BookingService bookingService, EventService eventService,
                           UserService userService, SeanceService seanceService) {
    this.bookingService = bookingService;
    this.eventService = eventService;
    this.userService = userService;
    this.seanceService = seanceService;
  }

  @RequestMapping
  public String showPage(Model model) {
    Collection<Event> events = eventService.getAll();
    model.addAttribute("events", events);
    return "booking";
  }

  @RequestMapping("/book-tickets")
  public String getTicketsPrice(Model model) {
    Collection<User> users = userService.getUsers();
    model.addAttribute("users", users);
    Collection<Event> events = eventService.getAll();
    model.addAttribute("events", events);

    return "book-tickets";
  }

  @RequestMapping("/book-tickets/book")
  public String bookTicket(Model model,
                           Principal principal, // user
                           @RequestParam("eventId") Long eventId,
                           @RequestParam(name = "airDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime airDate,
                           @RequestParam(name = "seats", required = false) String seats) {
    String email = principal.getName();

    User user = userService.getUserByEmail(email);
    Seance seance = seanceService.getSeanceByEventIdAndDateTime(eventId, airDate);
    Event event = seance.getEvent();
    Auditorium auditorium = event.getAuditorium();
    List<Seat> auditoriumSeats = auditorium.getSeats();

    model.addAttribute("selectedEvent", event);
    model.addAttribute("selectedAirDate", seance.getDateTime());

    if (seats != null && !seats.trim().isEmpty()) {

      List<Long> listOfSeats = Arrays.stream(seats.split(","))
          .map(s -> Long.parseLong(s.trim()))
          .collect(Collectors.toList());

      List<Seat> selectedSeats = auditoriumSeats.stream()
          .filter(s -> listOfSeats.contains(s.getNumber()))
          .collect(Collectors.toList());

      Set<Ticket> tickets = selectedSeats.stream()
          .map(seat -> new Ticket(seance, seat, user))
          .collect(Collectors.toSet());

      bookingService.bookTickets(tickets, user);

      log.info("Book {} tickets on {}:{} by {}:{} at {}",
          tickets.size(),
          event.getId(), event.getName(),
          user.getId(), user.getFirstName() + " " + user.getLastName(),
          listOfSeats);
    }

    return getTicketsPrice(model);
  }

  @RequestMapping(path = "/tickets", produces = "application/pdf")
  public ModelAndView showTickets(ModelAndView modelAndView,
                                  @RequestParam("seanceId") long seanceId) {

    Seance seance = seanceService.getById(seanceId);

    List<Ticket> purchasedTickets = seance.getTickets()
        .stream()
        .filter(t -> t.getUser() != null)
        .collect(Collectors.toList());

    modelAndView.addObject("tickets", purchasedTickets);
    modelAndView.setViewName("pdf");

    return modelAndView;
  }
}
