package com.epam.spring.hometask.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.epam.spring.hometask.controller")
public class ExceptionHandlerController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

  //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public String handleAllException(Model model, Exception e) {
    LOGGER.error("Error", e);
    model.addAttribute("errorMessage", e.getMessage());
    return "error";
  }

  @ExceptionHandler(AuthenticationException.class)
  public String handleCustomException(Model model, AuthenticationException e) {
    model.addAttribute("error", e.getMessage());
    return "login";
  }

  @ExceptionHandler(UsernameNotFoundException.class)
  public String handleUsernameNotFoundException(Model model, AuthenticationException e) {
    model.addAttribute("error", e.getMessage());
    return "login";
  }

}
