package com.epam.spring.hometask.view;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;
import com.epam.spring.hometask.domain.Ticket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Map;

public class PdfView extends AbstractPdfView {
  @Override
  protected void buildPdfDocument(Map<String, Object> model,
                                  Document document,
                                  PdfWriter writer,
                                  HttpServletRequest request, HttpServletResponse response) throws Exception {
    @SuppressWarnings("unchecked")
    List<Ticket> tickets = (List<Ticket>) model.get("tickets"); //(Set<Ticket>)

    Paragraph paragraph = new Paragraph("Purchased tickets");
    PdfPTable table = new PdfPTable(3);

    table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
    table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
    Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

    table.addCell(new Phrase("SeanceId", headerFont));
    table.addCell(new Phrase("Date and time", headerFont));
    table.addCell(new Phrase("Seat", headerFont));

    for (Ticket ticket : tickets) {
      table.addCell(ticket.getSeance().getId().toString());
      table.addCell(ticket.getSeance().getDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)));
      table.addCell(Long.toString(ticket.getSeat().getNumber()));
    }

    paragraph.add(table);
    document.add(paragraph);
  }

}

