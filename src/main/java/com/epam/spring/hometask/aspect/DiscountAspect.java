package com.epam.spring.hometask.aspect;

import com.epam.spring.hometask.strategy.DiscountStrategy;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.repository.CounterRepository;

@Aspect
@Component
public class DiscountAspect {

  private static final Logger log = LoggerFactory.getLogger(DiscountAspect.class);

  @Autowired
  private CounterRepository counterRepository;

  @Pointcut("execution(* com.epam.spring.hometask.service.DiscountService.getDiscount(..))")
  public void pointcutDiscount() {
  }

  @Around("pointcutDiscount()")
  public byte count(ProceedingJoinPoint jp) throws Throwable {
    //DiscountStrategy discountStrategyObject = (DiscountStrategy) jp.getThis();
    byte discount = (byte) jp.proceed();
    User user = (User) jp.getArgs()[0];

    if (discount != 0 && user != null) {
      String counterName = Counters.DISCOUNT_USER_ID.name() + "_" + user.getId();

      Long count = counterRepository.findByName(counterName).getCount();
      counterRepository.increment(counterName);

      log.info("{}: {}", counterName, count + 1);

      count = counterRepository.findByName(Counters.DISCOUNT_TOTAL.name()).getCount();
      counterRepository.increment(Counters.DISCOUNT_TOTAL.name());

      log.info("{}: {}", Counters.DISCOUNT_TOTAL, count + 1);
    }

    return discount;
  }
}
