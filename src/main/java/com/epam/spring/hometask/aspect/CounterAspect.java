package com.epam.spring.hometask.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.hometask.repository.CounterRepository;

@Aspect
@Component
public class CounterAspect {

    private static final Logger log = LoggerFactory.getLogger(CounterAspect.class);

    @Autowired
    private CounterRepository counterRepository;

    @Pointcut("execution(* com.epam.spring.hometask.service.EventService.getByName(..))")
    public void pointcutEventByName() {
    }

    @Pointcut("execution(* com.epam.spring.hometask.service.BookingService.getTicketsPrice(..))")
    public void pointcutPriceQueried() {
    }

    @Pointcut("execution(* com.epam.spring.hometask.service.BookingService.bookTickets(..))")
    public void pointcutTicketBooked() {
    }

    @AfterReturning("pointcutEventByName()")
    public void countEventAccessedByName() {
        long count = counterRepository.findByName(Counters.EVENT_ACCESSED_BY_NAME.name()).getCount() + 1;
        counterRepository.increment(Counters.EVENT_ACCESSED_BY_NAME.name());
        log.info("{}: {} time(s)", Counters.EVENT_ACCESSED_BY_NAME, count);
    }

    @AfterReturning("pointcutPriceQueried()")
    public void countEventPriceQueried() {
        long count = counterRepository.findByName(Counters.PRICE_QUERIED.name()).getCount() + 1;
        counterRepository.increment(Counters.PRICE_QUERIED.name());
        log.info(Counters.PRICE_QUERIED + ": " + count + " times");
    }

    @AfterReturning("pointcutTicketBooked()")
    public void countTicketBooked() {
        long count = counterRepository.findByName(Counters.TICKET_BOOKED.name()).getCount() + 1;
        counterRepository.increment(Counters.TICKET_BOOKED.name());
        log.info(Counters.TICKET_BOOKED + ": " + count + " times");
    }
}
