package com.epam.spring.hometask.aspect;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.domain.Win;
import com.epam.spring.hometask.repository.WinsRepository;

@Aspect
@Component
public class LuckyWinnerAspect {

  private static final BigDecimal WIN_PRICE = BigDecimal.valueOf(0F);
  private static final int LUCKY = 5;
  private static final Logger log = LoggerFactory.getLogger(LuckyWinnerAspect.class);
  private Random random = new Random();

  @Autowired
  private WinsRepository winsRepository;

  @Pointcut("execution(* com.epam.spring.hometask.service.BookingService.bookTickets(..))")
  public void pointcutLuckyWinner() {
  }

  @Around("pointcutLuckyWinner()")
  public Object luckyWinner(ProceedingJoinPoint jp) throws Throwable {
    List ticketsList = (List) jp.getArgs()[0];
    User user = (User) jp.getArgs()[1];

    List<Ticket> tickets = new ArrayList<>(ticketsList.size());

    for(Object obj : ticketsList) {
      tickets.add((Ticket) obj);
    }

    tickets = tickets.stream().filter(t -> this.checkLucky()).collect(Collectors.toList());

    if (tickets.size() > 0) {
      Win win = new Win();
      win.setDate(LocalDate.now());
      win.setUser(user);
      winsRepository.save(win);

      tickets.stream().forEach(t -> t.setPrice(WIN_PRICE));

      log.info("User {} {} is lucky winner", user.getFirstName(), user.getLastName());
    }

    return jp.proceed();
  }

  private boolean checkLucky() {
    int rnd = random.nextInt() % LUCKY;
    return rnd == 0;
  }

}
