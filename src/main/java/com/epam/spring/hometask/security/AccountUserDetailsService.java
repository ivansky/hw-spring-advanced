package com.epam.spring.hometask.security;

import com.epam.spring.hometask.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class AccountUserDetailsService implements UserDetailsService {

  @Autowired
  private UserService userService;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    com.epam.spring.hometask.domain.User user = userService.getUserByEmail(email);

    if(user == null) {
      throw new UsernameNotFoundException("User " + email + " not found.");
    }

    if (user.getRoles() == null || user.getRoles().isEmpty()) {
      // No Roles assigned to user...
      throw new UsernameNotFoundException("User not authorized. User hasn't roles.");
    }

    Collection<GrantedAuthority> grantedAuthorities = user.getRoles()
        .stream()
        .map(role -> new SimpleGrantedAuthority(role.getCode()))
        .collect(Collectors.toCollection(ArrayList::new));

    return new User(user.getEmail(),
        user.getPassword(), user.isEnabled(),
        !user.isExpired(), !user.isCredentialsExpired(),
        !user.isLocked(), grantedAuthorities);
  }

}
