package com.epam.spring.hometask.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.sql.Date;
import java.time.format.DateTimeFormatter;

@Converter(autoApply = true)
public class LocalDateAttributeConverter
    extends XmlAdapter<String, LocalDate>
    implements AttributeConverter<LocalDate, Date> {

  private DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;

  @Override
  public Date convertToDatabaseColumn(LocalDate locDate) {
    return (locDate == null ? null : Date.valueOf(locDate));
  }

  @Override
  public LocalDate convertToEntityAttribute(Date sqlDate) {
    return (sqlDate == null ? null : sqlDate.toLocalDate());
  }

  @Override
  public LocalDate unmarshal(String dateString) throws Exception {
    return formatter.parse(dateString, LocalDate::from);
  }

  @Override
  public String marshal(LocalDate date) throws Exception {
    return formatter.format(date);
  }

}