package com.epam.spring.hometask.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter
    extends XmlAdapter<String, LocalDateTime>
    implements AttributeConverter<LocalDateTime, Timestamp> {

  private DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

  @Override
  public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
    return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
  }

  @Override
  public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
    return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
  }

  @Override
  public LocalDateTime unmarshal(String dateTimeString) throws Exception {
    return formatter.parse(dateTimeString, LocalDateTime::from);
  }

  @Override
  public String marshal(LocalDateTime dateTime) throws Exception {
    return formatter.format(dateTime);
  }
}