package com.epam.spring.hometask.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateConverter {

  public static LocalDateTime toLocalDateTime(Date date) {
    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }

  public static LocalDate toLocalDate(Date date) {
    return toLocalDateTime(date).toLocalDate();
  }

  public static Date toDate(LocalDateTime dateTime) {
    return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
  }

  public static Timestamp toTimestamp(LocalDateTime dateTime){
    if(dateTime!=null){
      return Timestamp.valueOf(dateTime);
    }
    return null;
  }

  public static Timestamp toTimestamp(LocalDate dateTime){
    if(dateTime!=null){
      return Timestamp.valueOf(dateTime.atStartOfDay());
    }
    return null;
  }

}
