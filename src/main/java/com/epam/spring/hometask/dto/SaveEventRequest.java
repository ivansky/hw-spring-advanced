package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.Event;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SaveEventRequest {
  private Event event;

  public SaveEventRequest() {
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

}
