package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.Event;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GetEventByIdResponse {

  private Event event;

  public GetEventByIdResponse() {
  }

  public GetEventByIdResponse(Event event) {
    this.event = event;
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

}
