package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.Event;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement
public class GetEventsResponse {

  private Collection<Event> events;

  public GetEventsResponse() {
  }

  public GetEventsResponse(Collection<Event> events) {
    this.events = events;
  }

  public Collection<Event> getEvents() {
    return events;
  }

  public void setEvents(Collection<Event> events) {
    this.events = events;
  }


}
