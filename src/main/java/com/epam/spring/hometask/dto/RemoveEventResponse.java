package com.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RemoveEventResponse {
  private String message;

  public RemoveEventResponse() {
  }

  public RemoveEventResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
