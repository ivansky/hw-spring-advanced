package com.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GetEventByNameRequest {

  private String name;

  public GetEventByNameRequest() {
  }

  public GetEventByNameRequest(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
