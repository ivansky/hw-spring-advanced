package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.User;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SaveUserResponse {

  private User user;

  public SaveUserResponse() {
  }

  public SaveUserResponse(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

}
