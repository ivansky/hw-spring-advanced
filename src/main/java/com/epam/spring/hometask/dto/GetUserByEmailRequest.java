package com.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GetUserByEmailRequest {
  private String email;

  public GetUserByEmailRequest() {
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}
