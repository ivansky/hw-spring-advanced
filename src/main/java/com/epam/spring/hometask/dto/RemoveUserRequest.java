package com.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RemoveUserRequest {
  private Long userId;

  public RemoveUserRequest() {
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

}
