package com.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RemoveUserResponse {
  private String message;

  public RemoveUserResponse() {
  }

  public RemoveUserResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
