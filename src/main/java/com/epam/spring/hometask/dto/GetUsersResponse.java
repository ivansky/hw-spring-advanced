package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement
public class GetUsersResponse {

  private Collection<User> users;

  public GetUsersResponse() {
  }

  public GetUsersResponse(Collection<User> users) {
    this.users = users;
  }

  public Collection<User> getUsers() {
    return users;
  }

  public void setUsers(Collection<User> users) {
    this.users = users;
  }

}
