package com.epam.spring.hometask.dto;

import com.epam.spring.hometask.domain.User;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SaveUserRequest {
  private User user;

  public SaveUserRequest() {
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

}
