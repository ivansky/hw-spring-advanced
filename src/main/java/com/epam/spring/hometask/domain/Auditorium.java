package com.epam.spring.hometask.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "auditorium")
public class Auditorium extends AbstractPersistable<Long> {

  @Column
  private String name;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "auditoriumId", cascade = CascadeType.ALL)
  private List<Seat> seats;

  public Auditorium() {
  }

  public Auditorium(long id) {
    this.setId(id);
  }

  public Auditorium(long id, String name) {
    this(id);
    this.setName(name);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Seat> getSeats() {
    return seats;
  }

  public void setSeats(List<Seat> seats) {
    this.seats = seats;
  }

}
