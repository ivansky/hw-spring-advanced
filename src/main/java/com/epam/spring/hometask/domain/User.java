package com.epam.spring.hometask.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User extends AbstractPersistable<Long> implements Serializable {

  @Column
  private String firstName;

  @Column
  private String lastName;

  @NotNull
  @Column(nullable = false)
  private String email;

  @NotNull
  @Column(nullable = false)
  @JsonIgnore
  private String password;

  @NotNull
  @Column(nullable = false, columnDefinition="tinyint(1) default 1")
  private boolean enabled = true;

  @NotNull
  @Column(nullable = false, columnDefinition="tinyint(1) default 0")
  private boolean credentialsExpired = false;

  @NotNull
  @Column(nullable = false, columnDefinition="tinyint(1) default 0")
  private boolean expired = false;

  @NotNull
  @Column(nullable = false, columnDefinition="tinyint(1) default 0")
  private boolean locked = false;

  @Column
  private LocalDate birthday;

  @OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
  private UserAccount userAccount;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(
      name = "userRole",
      joinColumns = @JoinColumn(
          name = "userId",
          referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(
          name = "roleId",
          referencedColumnName = "id"))
  private Set<Role> roles;

  @OneToMany(targetEntity = Ticket.class, mappedBy = "user", cascade = CascadeType.ALL)
  private List<Ticket> tickets;

  public User() {
  }

  public User(Long id) {
    super(id);
  }

  public User(String firstName, String lastName, String email) {
    this.setFirstName(firstName);
    this.setLastName(lastName);
    this.setEmail(email);
  }

  public User(Long id, String firstName, String lastName, String email) {
    this(firstName, lastName, email);
    this.setId(id);
  }

  public User(String firstName, String lastName, String email, LocalDate birthday) {
    this(firstName, lastName, email);
    this.setBirthday(birthday);
  }

  public User(long id, String firstName, String lastName, String email, LocalDate birthday) {
    this(firstName, lastName, email, birthday);
    this.setId(id);
  }

  public UserAccount getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(UserAccount userAccount) {
    this.userAccount = userAccount;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  public List<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isCredentialsExpired() {
    return credentialsExpired;
  }

  public void setCredentialsExpired(boolean credentialsExpired) {
    this.credentialsExpired = credentialsExpired;
  }

  public boolean isExpired() {
    return expired;
  }

  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

}
