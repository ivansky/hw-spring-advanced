package com.epam.spring.hometask.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "event")
public class Event extends AbstractPersistable<Long> {

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private float basePrice;

  @ManyToOne
  @JoinColumn(name = "auditoriumId")
  private Auditorium auditorium;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinColumn(name = "eventId")
  private List<Seance> seances;

  @Enumerated(EnumType.STRING)
  private EventRating rating;

  public Event() {
  }

  public Event(long id, String name, float basePrice, EventRating rating) {
    this(name, basePrice, rating);
    this.setId(id);
  }

  public Event(String name, float basePrice, EventRating rating) {
    this.setName(name);
    this.setBasePrice(basePrice);
    this.setRating(rating);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public float getBasePrice() {
    return basePrice;
  }

  public void setBasePrice(float basePrice) {
    this.basePrice = basePrice;
  }

  public EventRating getRating() {
    return rating;
  }

  public void setRating(EventRating rating) {
    this.rating = rating;
  }

  public Auditorium getAuditorium() {
    return auditorium;
  }

  public void setAuditorium(Auditorium auditorium) {
    this.auditorium = auditorium;
  }

  public List<Seance> getSeances() {
    return seances;
  }

  public void setSeances(List<Seance> seances) {
    this.seances = seances;
  }
}
