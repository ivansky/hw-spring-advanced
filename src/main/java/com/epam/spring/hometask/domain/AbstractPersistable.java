package com.epam.spring.hometask.domain;

import javax.annotation.Nonnull;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.springframework.util.ClassUtils;

import java.io.Serializable;

/**
 * Abstract base class for entities. Allows parameterization of id type, chooses auto-generation and implements
 * {@link #equals(Object)} and {@link #hashCode()} based on that id.
 *
 * @author Oliver Gierke
 * @author Thomas Darimont
 */
@MappedSuperclass
public abstract class AbstractPersistable<PK extends Serializable> implements Persistable<PK> {

  private static final long serialVersionUID = -5554308939350869754L;

  @Id
  @GeneratedValue
  private PK id;

  public PK getId() {
    return id;
  }

  AbstractPersistable() {}

  AbstractPersistable(PK id) {
    setId(id);
  }

  /**
   * Sets the id of the entity.
   *
   * @param id the id to set
   */
  public void setId(@Nonnull final PK id) {
    this.id = id;
  }

  /**
   * Must be {@link Transient} in order to ensure that no JPA provider complains because of a missing setter.
   *
   * @apiNote DATAJPA-622
   * @see org.springframework.data.domain.Persistable#isNew()
   */
  @Transient
  public boolean isNew() {
    return null == getId();
  }

  @Override
  public String toString() {
    return String.format("Entity of type %s with id: %s", this.getClass().getName(), getId());
  }

  @Override
  public boolean equals(Object obj) {

    if (null == obj) {
      return false;
    }

    if (this == obj) {
      return true;
    }

    if (!getClass().equals(ClassUtils.getUserClass(obj))) {
      return false;
    }

    AbstractPersistable<?> that = (AbstractPersistable<?>) obj;

    return null != this.getId() && this.getId().equals(that.getId());
  }

  @Override
  public int hashCode() {

    int hashCode = 17;

    hashCode += null == getId() ? 0 : getId().hashCode() * 31;

    return hashCode;
  }
}