package com.epam.spring.hometask.domain;

import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "role")
public class Role extends AbstractPersistable<Long> {

  @NotNull
  private String code;

  @NotNull
  private String label;

  public Role() {
  }

  public Role(@Nonnull Long id) {
    setId(id);
  }

  public Role(String code) {
    this.setCode(code);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
