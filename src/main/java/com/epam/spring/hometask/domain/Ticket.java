package com.epam.spring.hometask.domain;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "ticket")
public class Ticket extends AbstractPersistable<Long> {

  @Column(nullable = false)
  private BigDecimal price;

  @OneToOne
  @JoinColumn(name = "seatId")
  private Seat seat;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "seanceId")
  private Seance seance;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "userId")
  private User user = null;

  @Column
  private LocalDateTime sellDate;

  public Ticket() {
  }

  public Ticket(Seance seance, Seat seat) {
    setSeance(seance);
    setSeat(seat);
  }

  public Ticket(Seance seance, Seat seat, User user) {
    this(seance, seat);
    setUser(user);
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Seat getSeat() {
    return seat;
  }

  public void setSeat(Seat seat) {
    this.seat = seat;
  }

  public Seance getSeance() {
    return seance;
  }

  public void setSeance(Seance seance) {
    this.seance = seance;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public LocalDateTime getSellDate() {
    return sellDate;
  }

  public void setSellDate(LocalDateTime sellDate) {
    this.sellDate = sellDate;
  }
}
