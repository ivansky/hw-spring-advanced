package com.epam.spring.hometask.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "userAccount")
public class UserAccount implements Serializable {

  @Id
  @Column(unique = true, nullable = false)
  private Long userId;

  @MapsId
  @OneToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="userId")
  private User user;

  @Column(nullable = false)
  private BigDecimal amount = BigDecimal.ZERO;

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
