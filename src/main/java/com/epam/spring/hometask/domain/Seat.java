package com.epam.spring.hometask.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(
    name = "seat",
    uniqueConstraints = @UniqueConstraint(columnNames = {"auditoriumId", "number"})
)
@Entity
public class Seat extends AbstractPersistable<Long> {

  @Column
  private long auditoriumId;

  @Column(nullable = false)
  private Long number;

  @Column(nullable = false)
  private boolean vip;

  public Seat() {
  }

  public Seat(long auditoriumId, long number, boolean vip) {
    this.auditoriumId = auditoriumId;
    this.number = number;
    this.vip = vip;
  }

  public Seat(long auditoriumId, long number) {
    this(auditoriumId, number, false);
  }

  public Long getNumber() {
    return number;
  }

  public void setNumber(Long number) {
    this.number = number;
  }

  public boolean isVip() {
    return vip;
  }

  public void setVip(boolean vip) {
    this.vip = vip;
  }

  public long getAuditoriumId() {
    return auditoriumId;
  }

  public void setAuditoriumId(long auditoriumId) {
    this.auditoriumId = auditoriumId;
  }
}
