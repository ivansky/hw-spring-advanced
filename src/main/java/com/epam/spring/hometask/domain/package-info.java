@XmlSchema(namespace = "http://localhost:8080/ws",
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlJavaTypeAdapters(value = {
        @XmlJavaTypeAdapter(value = LocalDateAttributeConverter.class, type = LocalDate.class),
        @XmlJavaTypeAdapter(value = LocalDateTimeAttributeConverter.class, type = LocalDateTime.class)
})
@XmlAccessorType(XmlAccessType.FIELD)
package com.epam.spring.hometask.domain;

import com.epam.spring.hometask.util.LocalDateAttributeConverter;
import com.epam.spring.hometask.util.LocalDateTimeAttributeConverter;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NavigableMap;